import 'package:flutter/material.dart';

void main() {
  runApp(const app());
}

class app extends StatelessWidget {
  const app({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Scaffold(
          appBar: AppBar(
            leading: Icon(Icons.account_circle_outlined),
            title: Text('First App'),
            actions: [
              IconButton(onPressed: () {},
                  icon: Icon(Icons.account_balance)),
              IconButton(onPressed: () {},
                  icon: Icon(Icons.ac_unit_sharp))
            ],
          ),
          body: Center(
            child: Column(
              children: [
                // Image.asset('assets/nat.jpg',
                // height: 300,
                // width: 250,),
                CircleAvatar(
                  backgroundImage: AssetImage('assets/nat.jpg'),
                  radius: 150,
                ),
                Text('Nattapat Chaiphet',
                    style: TextStyle(
                        color: Colors.green,
                        fontWeight: FontWeight.w600,
                        fontSize: 45)),
                Text('6350110023',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontSize: 25)),
              ],
            ),
          )),
    );
  }
}
